require('dotenv').config();
const mongoose = require('mongoose');

const Product = require('./models/product');

mongoose.connect(//all connection logic
    process.env.DB_URL
).catch(() => {
    console.log('Connection failed.');
});

const createProduct = async (req, res, next) => {
    
    const createdProduct = new Product({
        name: req.body.name,
        price: req.body.price
    });
    const result = await createdProduct.save();

    res.json(result);
};

const getProducts = async (req, res, next) => {

   const products = await Product.find().exec(); //returns array by default

    res.json(products);
};

exports.createProduct = createProduct;
exports.getProducts = getProducts;