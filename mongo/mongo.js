require('dotenv').config();
const MongoClient = require('mongodb').MongoClient;

const url = process.env.DB_URL;

const createProduct = async (req, res, next) => {
    const newProduct = {
        name: req.body.name,
        price: req.body.price
    };
    // Tells Mongo to which server shall we connect
    const client = new MongoClient(url);

    try {
        await client.connect();
        const db = client.db(); //No arguments will take the url string
        const result = await db.collection('product').insertOne(newProduct);
        console.log(result);
    }catch(error){
        return res.json({message: 'Could not store data.'});
    };
    client.close(); //close connection

    res.json(newProduct);
};

const getProducts = async (req, res, next) => {

    const client = new MongoClient(url);

    let products;

    try {
        await client.connect();
        const db = client.db(); //No arguments will take the url string
        products = await db.collection('product').find().toArray();
    }catch(error){
        return res.json({message: 'Could not retrieve data.'});
    };

    res.json(products);

};

exports.createProduct = createProduct;
exports.getProducts = getProducts;