const express = require('express');

const placesControllers = require('../controllers/places-controller');

const { newPlaceValidator, updatedPlaceValidator } = require('../validators/places-validator');

const router = express.Router();


router.get('/', placesControllers.getAllPlaces);
router.get('/:pid', placesControllers.getPlaceById);
router.get('/user/:uid', placesControllers.getPlacesByUserId);

router.post('/', newPlaceValidator(), placesControllers.createPlace);

router.patch('/:pid', updatedPlaceValidator(), placesControllers.updatePlace);

router.delete('/:pid', placesControllers.deletePlace);


module.exports = router;