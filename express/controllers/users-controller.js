const HttpError = require('../models/http-error');

const DUMMY_USERS = [
    {
        id: 1,
        name: 'Mauricio',
        email: 'mauricio@gmail.com',
        pass: '123456'
    },
    {
        id: 2,
        name: 'Maria',
        email: 'maria@gmail.com',
        pass: 'qwerty'
    }
]

const getUsers = (req, res, next) => {
    res.json({users: DUMMY_USERS});
};

const signup = (req, res, next) => {

    const { name, email, password } = req.body;

    if (DUMMY_USERS.find(user => user.email === email)){
        throw new HttpError('Email used previously');
    }

    const lastId = DUMMY_USERS.length;

    const newUser = {
        id: lastId+1,
        name: name,
        email: email,
        password: password
    }

    DUMMY_USERS.push(newUser);

    res.status(201).json({user: newUser});

};

const login = (req, res, next) => {

    const { email, password } = req.body;

    const loggedUser = DUMMY_USERS.find(user => (user.email === email && user.password === password));

    if (loggedUser) {
        res.json({message: 'Logged in!'})
    }else{
        throw new HttpError('Could not identify user');
    }

};

exports.getUsers = getUsers;
exports.signup = signup;
exports.login = login;