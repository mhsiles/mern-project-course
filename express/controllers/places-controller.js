const HttpError = require('../models/http-error');
const { validationResult } = require('express-validator');

let dummyPlaces = [
    {
        id: 1,
        name: 'Empire State',
        city: 'New York',
        creator: 'u1'
    },
    {
        id: 2,
        name: 'Chichen-Itza',
        city: 'Yucatán',
        creator: 'u2'
    },
    {
        id: 3,
        name: 'Eiffel Tower',
        city: 'Paris',
        creator: 'u3'
    },
]

const getAllPlaces = (req, res, next) => {

    if (!dummyPlaces) {
        return next(
            new HttpError('Could not find any places.', 404)
        );
    }
    
    res.json({dummyPlaces});

};

const getPlaceById = (req, res, next) => {

    const placeId = req.params.pid;
    // find by id
    const place = dummyPlaces.find(p => p.id == placeId);

    if (!place) {
        return next(
            new HttpError('Could not find a place for the id.', 404)
        );
    }
    
    res.json({place});

};

const getPlacesByUserId = (req, res, next) => {
    const userId = req.params.uid;

    const place = dummyPlaces.find(p => p.creator == userId);

    if (!place) {
        return next(
            new HttpError('Could not find a place for the provided user id.', 404)
        );
    }

    res.json({place});
};

const createPlace = (req, res, next) => {

    const errors = validationResult(req);

    if (!errors.isEmpty()){
        throw new HttpError('Invalid input data', 422);
    }

    const { name, city, creator } = req.body;

    const lastId = dummyPlaces.length;

    const createdPlace = {
        id: lastId+1,
        name,
        city,
        creator
    };

    dummyPlaces.push(createdPlace);

    res.status(201).json({place: createdPlace});

};

const updatePlace = (req, res, next) => {

    const { name, city } = req.body;
    const placeId = req.params.pid;

    let updatedPlace = { ...dummyPlaces.find(place => place.id === parseInt(placeId)) };
    const placeIndex = dummyPlaces.findIndex(p => p.id === parseInt(placeId));

    updatedPlace.name = name;
    updatedPlace.city = city;

    dummyPlaces[placeIndex] = updatedPlace;

    res.status(200).json({place: updatedPlace});

};

const deletePlace = (req, res, next) => {

    const placeId = req.params.pid;

    dummyPlaces = dummyPlaces.filter(place => place.id != parseInt(placeId));

    res.status(200).json({message: 'Success deletion.'})

};

exports.getAllPlaces = getAllPlaces;
exports.getPlaceById = getPlaceById;
exports.getPlacesByUserId = getPlacesByUserId;
exports.createPlace = createPlace;
exports.updatePlace = updatePlace;
exports.deletePlace = deletePlace;