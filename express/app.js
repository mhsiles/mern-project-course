// Third party libraries
const express = require('express');
const bodyParser = require('body-parser');
const HttpError = require('./models/http-error');
// File imports
const placesRoutes = require('./routes/places-routes');
const usersRoutes = require('./routes/users-routes');

// Initialize app
const app = express();
const port = process.env.PORT;

// Parse any incoming request body and calls next
app.use(bodyParser.json());

app.use('/api/users', usersRoutes);
app.use('/api/places', placesRoutes);

app.use((req, res, next) => {
    const error = new HttpError('Page not found', 404);
    throw error;
});

// Will execute if response contains error
app.use((error, req, res, next) => {
    if (res.headerSent)
        return next(error);

    res.status(error.code || 500);
    res.json({ message: error.message || 'An unknown error occurred.' })

});

app.listen(port, () => {
    console.log('Server started at port ' + port);
});