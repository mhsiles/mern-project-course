const { check } = require('express-validator'); //validate forms

const newPlaceValidator = () => {
    return [
        //name
        check('name').exists(),
        check('name').notEmpty(),
        check('name').isLength({min: 5, max: 20}),
        //city
        check('city').exists(),
        check('city').notEmpty(),
        check('city').isLength({min: 3, max: 30}),
        //creator
        check('creator').exists(),
        check('creator').notEmpty(),
        check('creator').isLength({min: 2, max: 5})
    ];
};

const updatedPlaceValidator = () => {
    return [
        //name
        check('name').notEmpty(),
        check('name').isLength({min: 5, max: 20}),
        //city
        check('city').notEmpty(),
        check('city').isLength({min: 3, max: 30}),
        //creator
        check('creator').notEmpty(),
        check('creator').isLength({min: 2, max: 5})
    ];
};

module.exports = {
    newPlaceValidator,
    updatedPlaceValidator
}